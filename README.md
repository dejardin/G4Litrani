# G4Litrani
Small program to generate shower in CMS-ECAL crystals and write 
results on files to feed SLitrani.

The Cerenkov photons generation is switched ON

2 files are written :
- Cerenkov.xxx.data
- Edep.xxx.data

which contain the position, direction, time of creation and energy of each photon or energy deposition in crystals.
Have a look at src/HistoManager.cc

To use it : 
- cd PbWO4_build
- rm -vf *
- cmake ../PbWO4
- make
- Edit cms.mac and modify at your wish
- ./PbWO4 cms.mac > /dev/null

M.D. 2018/03/13
