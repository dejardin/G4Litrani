#!/bin/sh
#-----------------------------------------------------------------------
# Resource file paths
# - Datasets
# geant4-parfullcms
# geant4data
geant4_datadir=/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/
echo $geant4_datadir

export G4NEUTRONHPDATA="`cd $geant4_datadir/geant4-G4NDL/4.5/data/G4NDL4.5 > /dev/null ; pwd`"
export G4LEDATA="`cd $geant4_datadir/geant4-G4EMLOW/7.3/data/G4EMLOW7.3 > /dev/null ; pwd`"
export G4LEVELGAMMADATA="`cd $geant4_datadir/geant4-G4PhotonEvaporation/5.2/data/PhotonEvaporation5.2 > /dev/null ; pwd`"
export G4RADIOACTIVEDATA="`cd $geant4_datadir/geant4-G4RadioactiveDecay/5.2/data/RadioactiveDecay5.2 > /dev/null ; pwd`"
export G4NEUTRONXSDATA="`cd $geant4_datadir/geant4-G4NEUTRONXS/1.4/data/G4NEUTRONXS1.4 > /dev/null ; pwd`"
export G4SAIDXSDATA="`cd $geant4_datadir/geant4-G4SAIDDATA/1.1/data/G4SAIDDATA1.1 > /dev/null ; pwd`"
export G4ABLADATA="`cd $geant4_datadir/geant4-G4ABLA/3.0/data/G4ABLA3.0 > /dev/null ; pwd`"
export G4ENSDFSTATEDATA="`cd $geant4_datadir/geant4-G4ENSDFSTATE/2.2/data/G4ENSDFSTATE2.2 > /dev/null ; pwd`"
export G4PIIDATA="`cd $geant4_datadir/geant4-/data/G4PII1.3 > /dev/null ; pwd`"
export G4REALSURFACEDATA="`cd $geant4_datadir/geant4-/data/RealSurface2.1 > /dev/null ; pwd`"


# - Fonts for Freetype
# FREETYPE SUPPORT NOT AVAILABLE

#----------------------------------------------------------------------

