//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file electromagnetic/TestEm9/src/HistoManager.cc
/// \brief Implementation of the HistoManager class
//
// $Id: HistoManager.cc 100809 2016-11-02 15:02:53Z gcosmo $
//
//---------------------------------------------------------------------------
//
// ClassName:   HistoManager
//
//
// Author:      V.Ivanchenko 30/01/01
//
//----------------------------------------------------------------------------
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "HistoManager.hh"
#include "G4MaterialCutsCouple.hh"
#include "G4EmProcessSubType.hh"
#include "G4VProcess.hh"
#include "G4VEmProcess.hh"
#include "G4VEnergyLossProcess.hh"
#include "G4UnitsTable.hh"
#include "Histo.hh"
#include "G4Gamma.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4OpticalPhoton.hh"
#include "G4SystemOfUnits.hh"

using namespace CLHEP;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

HistoManager* HistoManager::fManager = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

HistoManager* HistoManager::GetPointer()
{
  if(!fManager) {
    fManager = new HistoManager();
  }
  return fManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

HistoManager::HistoManager()
 : fGamma(0),
   fElectron(0),
   fPositron(0),
   fHisto(0)
{
  fVerbose = 2;
  fEvt1    = -1;
  fEvt2    = -1;
  fNmax    = 3;
  fMaxEnergy = 50.0*MeV;
  fBeamEnergy= 1.*GeV;
  fMaxEnergyAbs = 10.*MeV;
  fBinsE = 100;
  fBinsEA= 40;
  fBinsED= 100;
  fNHisto = 10;

  fHisto   = new Histo();
  BookHisto();

  fGamma = G4Gamma::Gamma();
  fElectron = G4Electron::Electron();
  fPositron = G4Positron::Positron();
  //fCerenkov=0x13b4bf0;
  fOptical=G4OpticalPhoton::OpticalPhoton();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

HistoManager::~HistoManager()
{
  delete fHisto;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::BookHisto()
{
  fHisto->Add1D("0","Evis in central crystal",fBinsED,0.0,1,1.0);
  fHisto->Add1D("1","Energy (MeV) of delta-electrons",
                fBinsE,0.0,fMaxEnergy,MeV);
  fHisto->Add1D("2","Energy (MeV) of gammas",fBinsE,0.0,800.0,MeV);
  fHisto->Add1D("3","dE (MeV) of steps",fBinsE,0.0,fMaxEnergy,keV);
  fHisto->Add1D("4","WL of cerenkov photons",fBinsE,300.0,900.0,1.);
  G4cout << "MeV = "<< MeV << " ev= "<< eV << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::BeginOfRun(G4int runID)
{
  // initilise scoring
  fEvt  = 0;
  fElec = 0;
  fPosit= 0;
  fGam  = 0;
  fLowe = 0;

  for(G4int i=0; i<6; i++) {
    fStat[i] = 0;
    fEdep[i] = 0.0;
    fErms[i] = 0.0;
    if(i < 3) {
      fEdeptr[i] = 0.0;
      fErmstr[i] = 0.0;
    }
  }

  // initialise counters
  fBrem.resize(93,0.0);
  fPhot.resize(93,0.0);
  fComp.resize(93,0.0);
  fConv.resize(93,0.0);

  // initialise acceptance - by default is not applied
  for(G4int i=0; i<fNmax; i++) {
    fEdeptrue[i] = 1.0;
    fRmstrue[i]  = 1.0;
    fLimittrue[i]= 10.;
  }

  if(fHisto->IsActive()) { 
    for(G4int i=0; i<fNHisto; ++i) {fHisto->Activate(i, true); }
    fHisto->Book();

    if(fVerbose > 0) {
      G4cout << "HistoManager: Histograms are booked and run has been started"
             << G4endl;
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::EndOfRun(G4int runID)
{

  G4cout << "HistoManager: End of run actions are started   RunID# " 
         << runID << G4endl;
  G4String nam[6] = {"1x1", "3x3", "5x5", "E1/E9 ", "E1/E25", "E9/E25"};

  // average

  G4cout<<"=================================================================" <<G4endl;
  G4double x = (G4double)fEvt;
  if(fEvt > 0) x = 1.0/x;
  G4int j;
  // total mean
  fEdep[0] *= x;
  G4double y = fErms[0]*x - fEdep[0]*fEdep[0];
  if(y < 0.0) y = 0.0;
  fErms[0] = std::sqrt(y);

  G4double xe = x*(G4double)fElec;
  G4double xg = x*(G4double)fGam;
  G4double xp = x*(G4double)fPosit;
  G4double xc = x*(G4double)fCerenkov;
  G4double xs = x*fStep;

  G4double f = 100.*std::sqrt(fBeamEnergy/GeV);

  G4cout                         << "Number of events             " << fEvt <<G4endl;
  G4cout << std::setprecision(4) << "Average number of e-         " << xe << G4endl;
  G4cout << std::setprecision(4) << "Average number of gamma      " << xg << G4endl;
  G4cout << std::setprecision(4) << "Average number of e+         " << xp << G4endl;
  G4cout << std::setprecision(4) << "Average number of Cerenkov   " << xc << G4endl;
  G4cout << std::setprecision(4) << "Average number of steps      " << xs << G4endl;
  
  G4double ex = fEdep[0];
  G4double sx = fErms[0];
  G4double xx= G4double(fStat[0]);
  if(xx > 0.0) xx = 1.0/xx;
  G4double rx = sx*std::sqrt(xx);
  G4cout << std::setprecision(4) << "Edep " << nam[0]
         << " =                   " << ex
         << " +- " << rx;
  if(ex > 0.1) G4cout << "  res=  " << f*sx/ex << " %   " << fStat[0];
  G4cout << G4endl;

  if(fLimittrue[0] < 10. || fLimittrue[1] < 10. || fLimittrue[2] < 10.) {
    G4cout <<"===========  Mean values without truncating ====================="<<G4endl;
    ex = fEdep[0];
    sx = fErms[0];
    rx = sx*std::sqrt(x);
    G4cout << std::setprecision(4) << "Edep " << nam[0]
           << " =                   " << ex
           << " +- " << rx;
     if(ex > 0.0) G4cout << "  res=  " << f*sx/ex << " %";
    G4cout << G4endl;
  }
  G4cout <<"===========  Ratios without truncating ==========================="<<G4endl;
  G4cout << std::setprecision(4) << "Beam Energy                  " << fBeamEnergy/GeV << " GeV" << G4endl;
  if(fLowe > 0)          G4cout << "Number of events E/E0<0.8    " << fLowe << G4endl; 
  G4cout <<"=================================================================="<<G4endl;
  G4cout<<G4endl;

  // normalise histograms
  if(fHisto->IsActive())
  { 
    for(G4int i=0; i<fNHisto; i++)
    {
      fHisto->ScaleH1(i,x);
    }
    fHisto->Save();
  }
  if(0 < runID) { return; }

  // atom frequency
  G4cout << "   Z  bremsstrahlung photoeffect  compton    conversion" << G4endl;
  for(j=1; j<93; ++j)
  {
    G4int n1 = G4int(fBrem[j]*x);
    G4int n2 = G4int(fPhot[j]*x);
    G4int n3 = G4int(fComp[j]*x);
    G4int n4 = G4int(fConv[j]*x);
    if(n1 + n2 + n3 + n4 > 0)
    {
      G4cout << std::setw(4) << j << std::setw(12) << n1 << std::setw(12) << n2
             << std::setw(12) << n3 << std::setw(12) << n4 << G4endl;
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::BeginOfEvent()
{
  ++fEvt;
  for (G4int i=0; i<25; i++)
  {
    fE[i] = 0.0;
  }
// Open files to save Photon and E deposition 
  char fname[132];
<<<<<<< HEAD
  //sprintf(fname,"data/Cerenkov.%s.%3.3d.data",fHisto->GetSufdata().data(),fEvt);
  sprintf(fname,"data/Cerenkov.%s.data",fHisto->GetSufdata().data());
  fd_Cerenkov=fopen(fname,"a");
  //sprintf(fname,"data/Edep.%s.%3.3d.data",fHisto->GetSufdata().data(),fEvt);
  sprintf(fname,"data/Edep.%s.data",fHisto->GetSufdata().data());
=======
  sprintf(fname,"Cerenkov.%s.data",fHisto->GetSufdata().data());
  fd_Cerenkov=fopen(fname,"a");
  sprintf(fname,"Edep.%s.data",fHisto->GetSufdata().data());
>>>>>>> bf50cb3fadb55fcf94a1cbe13c25ca524fc35831
  fd_Edep=fopen(fname,"a");
  fCerenkov  = 0;
  fStep = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::EndOfEvent()
{
  fE[0] /= fBeamEnergy;
  fStat[0]++;

  // compute ratios
  G4double e0 = fE[0];

  // Fill histo
  fHisto->Fill(1,e0,1.0);

  fEdep[0] += e0;
  fErms[0] += e0*e0;
  fclose(fd_Cerenkov);
  fclose(fd_Edep);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::ScoreNewTrack(const G4Track* aTrack)
{
  //Save primary parameters
  ResetTrackLength();
  const G4ParticleDefinition* particle = aTrack->GetDefinition();
  const G4DynamicParticle* dynParticle = aTrack->GetDynamicParticle();

  G4int pid = aTrack->GetParentID();
  G4double kinE = dynParticle->GetKineticEnergy();
  G4double Time = aTrack->GetGlobalTime();
  G4ThreeVector pos = aTrack->GetVertexPosition();
  G4ThreeVector dir = aTrack->GetMomentumDirection();

// primary
  if(0 == pid)
  {
    G4double mass = 0.0;
    if(particle) { mass = particle->GetPDGMass(); }
    if(1 < fVerbose)
    {
      G4cout << "TrackingAction: Primary kinE(MeV)= " << kinE/MeV
           << "; m(MeV)= " << mass/MeV
           << "; pos= " << pos << ";  dir= " << dir << G4endl;
    }
  }
// secondary
  else
  {
    const G4VProcess* proc = aTrack->GetCreatorProcess();
    G4int type = proc->GetProcessSubType();
    if(type == fBremsstrahlung)
    {
      const G4Element* elm = static_cast<const G4VEnergyLossProcess*>(proc)->GetCurrentElement();
      if(elm)
      {
        G4int Z = G4lrint(elm->GetZ());
        if(Z > 0 && Z < 93) { fBrem[Z] += 1.0; }
      }
    }
    else if(type == fPhotoElectricEffect)
    {
      const G4Element* elm = static_cast<const G4VEmProcess*>(proc)->GetCurrentElement();
      if(elm)
      {
        G4int Z = G4lrint(elm->GetZ());
        if(Z > 0 && Z < 93) { fPhot[Z] += 1.0; }
      }
    }
    else if(type == fGammaConversion)
    {
      const G4Element* elm = static_cast<const G4VEmProcess*>(proc)->GetCurrentElement();
      if(elm)
      {
        G4int Z = G4lrint(elm->GetZ());
        if(Z > 0 && Z < 93) { fConv[Z] += 1.0; }
      }
    }
    else if(type == fComptonScattering)
    {
      const G4Element* elm = static_cast<const G4VEmProcess*>(proc)->GetCurrentElement();
      if(elm)
      {
        G4int Z = G4lrint(elm->GetZ());
        if(Z > 0 && Z < 93) { fComp[Z] += 1.0; }
      }
    }

    // delta-electron
    if (particle == fElectron)
    {
      if(1 < fVerbose)
      {
        G4cout << "TrackingAction: Secondary electron " << G4endl;
      }
      AddDeltaElectron(dynParticle);

    }
    else if (particle == fPositron)
    {
      if(1 < fVerbose)
      {
        G4cout << "TrackingAction: Secondary positron " << G4endl;
      }
      AddPositron(dynParticle);

    }
    else if (particle == fGamma)
    {
      if(1 < fVerbose)
      {
        G4cout << "TrackingAction: Secondary gamma; parentID= " << pid
               << " E= " << kinE << G4endl;
      }
      AddPhoton(dynParticle);
    }
    else if (particle == fOptical)
    {
      if(1 < fVerbose)
      {
        // E=h nu= hc/lambda -> lambda=hc/E=6.63e-34*3.e8/1.6e-19/E(eV)=
        //G4cout << "Cerenkov photon; parentID= " << pid
        //       << " E= " << kinE << " WL= " << 1243.12/(kinE/eV) << " nm, "
        //       << " vertex : "<< pos << ", Direction : " << dir
        //       << ", proper time : " << Time/ps << " ps" << G4endl;
      }
      AddCerenkovPhoton(dynParticle);
      fprintf(fd_Cerenkov,"%d %e %e %e %e %e %e %e %e\n",fCerenkov,Time/ps,kinE/MeV,pos[0],pos[1],pos[2],dir[0],dir[1],dir[2]);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void HistoManager::AddEnergy(G4double edep, G4int volIndex, G4int copyNo, const G4Track *aTrack)
{
  if(1 < fVerbose)
  {
    G4cout << "HistoManager::AddEnergy: e(keV)= " << edep/keV
           << "; volIdx= " << volIndex
           << "; copyNo= " << copyNo
           << G4endl;
  }
  G4double Time = aTrack->GetGlobalTime();
  G4ThreeVector pos = aTrack->GetVertexPosition();
  G4ThreeVector dir = aTrack->GetMomentumDirection();
  if(0 == volIndex)
  {
    fE[copyNo] += edep;
    G4cout << "Cumulative energy : " << fE[copyNo]/MeV << " MeV";
    fprintf(fd_Edep,"%d %e %e %e %e %e %e %e %e\n",fStep,Time/ps,edep/MeV,pos[0],pos[1],pos[2],dir[0],dir[1],dir[2]);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::AddDeltaElectron(const G4DynamicParticle* elec)
{
  G4double e = elec->GetKineticEnergy()/MeV;
  if(e > 0.0) fElec++;
  fHisto->Fill(1,e,1.0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::AddPhoton(const G4DynamicParticle* ph)
{
  G4double e = ph->GetKineticEnergy()/MeV;
  if(e > 0.0) fGam++;
  fHisto->Fill(2,e,1.0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::AddCerenkovPhoton(const G4DynamicParticle* ph)
{
  G4double e = ph->GetKineticEnergy();
  if(e > 0.0) fCerenkov++;
  fHisto->Fill(4,1243.12/(e/eV),1.0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void HistoManager::SetEdepAndRMS(G4int i, G4ThreeVector val)
{
  if(i<fNmax && i>=0) {
    if(val[0] > 0.0) fEdeptrue[i] = val[0];
    if(val[1] > 0.0) fRmstrue[i] = val[1];
    if(val[2] > 0.0) fLimittrue[i] = val[2];
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

