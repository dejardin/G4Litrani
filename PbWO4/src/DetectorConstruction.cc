//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file electromagnetic/TestEm9/src/DetectorConstruction.cc
/// \brief Implementation of the DetectorConstruction class
//
// $Id: DetectorConstruction.cc 101905 2016-12-07 11:34:39Z gunter $
//
//
/////////////////////////////////////////////////////////////////////////
//
// TestEm9: Crystal calorimeter
//
// Created: 31.01.03 V.Ivanchenko
//
// Modified:
//
////////////////////////////////////////////////////////////////////////
//


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "DetectorConstruction.hh"

#include "G4TwoVector.hh"
#include "G4Box.hh"
#include "G4GenericTrap.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"

#include "G4TransportationManager.hh"

#include "G4GeometryManager.hh"
#include "G4RunManager.hh"
#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4ProductionCuts.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4NistManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fLogicWorld(0),
    fLogicCrystal(0)
{

  fWorldZ       = 0.0;
  fLogicWorld   = NULL;
  fLogicCrystal = NULL;

  DefineMaterials();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  return ConstructVolumes();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::DefineMaterials()
{
  // Default materials

  G4NistManager* man = G4NistManager::Instance();
  //  man->SetVerbose(1);
  fWorldMaterial = man->FindOrBuildMaterial("G4_AIR");
  fECALMaterial   = man->FindOrBuildMaterial("G4_PbWO4");

  const G4int NUMENTRIES = 2;

  G4double ppckov[NUMENTRIES] =     {1.5*eV, 3.3*eV};
  G4double rindex[NUMENTRIES] =     {2.24,   2.24};
  G4double absorption[NUMENTRIES] = {300*cm, 300*cm};

  G4MaterialPropertiesTable *MPT = new G4MaterialPropertiesTable();
  MPT->AddProperty("RINDEX",ppckov,rindex,NUMENTRIES);
  //MPT->AddProperty("ABSLENGTH",ppckov,absorption,NUMENTRIES);
  fECALMaterial->SetMaterialPropertiesTable(MPT);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::ConstructVolumes()
{

  G4int X_type=10;
  const G4double AF[]={21.83,21.83,21.83,21.83,21.83,21.83,21.83,21.83,21.83,21.83,
                     21.83,21.83,21.83,21.83,21.83,21.83,21.83,28.69};
  const G4double BF[]={23.59,22.22,22.34,22.47,22.61,22.60,22.55,22.67,22.82,23.08,
                     23.14,23.29,23.47,23.71,23.88,24.06,24.29,28.69};
  const G4double CF[]={21.85,21.87,21.91,21.94,21.97,22.00,22.03,22.05,22.08,22.10,
                     22.12,22.14,22.15,22.17,22.18,22.20,22.21,28.69};
  const G4double AR[]={25.84,25.81,25.75,25.67,25.56,25.43,25.29,25.14,24.98,24.82,
                     24.65,24.49,24.33,24.17,24.02,23.88,23.74,30.0};
  const G4double BR[]={25.48,26.22,26.28,26.32,26.34,26.18,25.96,25.92,25.90,26.00,
                     25.89,25.86,25.87,25.95,25.96,25.99,26.07,30.0};
  const G4double CR[]={25.86,25.86,25.84,25.80,25.72,25.63,25.52,25.39,25.26,25.12,
                     24.97,24.83,24.68,24.54,24.40,24.27,24.15,30.0};
  G4double dlength=115.;
  if(X_type==18)dlength=110.;
  G4int loc_type=X_type-1;
  G4double vertex[8][3];
  vertex[0][0]=0.;           vertex[0][1]=0.;           vertex[0][2]=-dlength;
  vertex[1][0]=0.;           vertex[1][1]=BF[loc_type]; vertex[1][2]=-dlength;
  vertex[2][0]=AF[loc_type]; vertex[2][1]=BF[loc_type]; vertex[2][2]=-dlength;
  vertex[3][0]=CF[loc_type]; vertex[3][1]=0.;           vertex[3][2]=-dlength;
  vertex[4][0]=0.;           vertex[4][1]=0.;           vertex[4][2]=dlength;
  vertex[5][0]=0.;           vertex[5][1]=BR[loc_type]; vertex[5][2]=dlength;
  vertex[6][0]=AR[loc_type]; vertex[6][1]=BR[loc_type]; vertex[6][2]=dlength;
  vertex[7][0]=CR[loc_type]; vertex[7][1]=0.;           vertex[7][2]=dlength;

  std::vector<G4TwoVector> coord;
  for(int i=0; i<8; i++)
  {
    coord.push_back(G4TwoVector(vertex[i][0],vertex[i][1]));
  }


  // Cleanup old geometry

  G4GeometryManager::GetInstance()->OpenGeometry();

  if(G4NistManager::Instance()->GetVerbose() > 0)
    G4cout << *(G4Material::GetMaterialTable()) << G4endl;

  G4SolidStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4PhysicalVolumeStore::GetInstance()->Clean();

  G4double CrystalLength   = 23.*cm;
  G4double CrystalWidth    = 2.5*cm;
  G4double gap    = 0.01*mm;
  G4double biggap = 2.*cm;

  fWorldZ = CrystalLength/2.+ biggap*2.;
  G4double worldX = CrystalWidth*3.0;
  G4double ecalZ  = 0.;

// World
  G4Box* World_Box            = new G4Box("World",worldX,worldX,fWorldZ);
  fLogicWorld                = new G4LogicalVolume(World_Box,fWorldMaterial,"World");
  G4VPhysicalVolume* world   = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.), "World",fLogicWorld,NULL,false,0);

// Ecal
  G4Box* ECAL_Box            = new G4Box("ECAL",worldX,worldX,CrystalLength/2. + gap);
  G4LogicalVolume* logicECAL = new G4LogicalVolume(ECAL_Box,fWorldMaterial,"ECAL");
  G4VPhysicalVolume* ECAL    = new G4PVPlacement(0,G4ThreeVector(0.,0.,ecalZ), "ECAL",logicECAL,world,false,0);

// Crystals
  G4GenericTrap *Crystal_Box = new G4GenericTrap("Crystal", dlength, coord);
  fLogicCrystal              = new G4LogicalVolume(Crystal_Box,fECALMaterial,"Crystal");
  G4VPhysicalVolume* Crystal = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),"Ecal",fLogicCrystal, ECAL,false,0);

  G4cout << "Ecal is " << G4BestUnit(CrystalLength,"Length") << " of " << fECALMaterial->GetName() << G4endl;

  // Crystals
  G4double x0 = -(CrystalWidth + gap)*2.0;
  G4double y  = x0;
  G4double x;
  G4int k = 0;
  G4int i,j;

  for (i=0; i<5; i++) {
    x  = x0;
    for (j=0; j<5; j++) {

      //new G4PVPlacement(0,G4ThreeVector(x,y,0.),"Ecal",fLogicCrystal, ECAL,false,k);
      k++;
      x += CrystalWidth + gap;
    }
    y += CrystalWidth + gap;
  }

  // color regions
  logicECAL-> SetVisAttributes(G4VisAttributes::GetInvisible());

  G4VisAttributes* regWcolor = new G4VisAttributes(G4Colour(0.3, 0.3, 0.3));
  fLogicWorld->SetVisAttributes(regWcolor);

  G4VisAttributes* regCcolor = new G4VisAttributes(G4Colour(0., 0.7, 0.3));
  fLogicCrystal->SetVisAttributes(regCcolor);

  // always return world
  G4cout << "### New geometry is constructed" << G4endl;
 
  return world;
}

void DetectorConstruction::UpdateGeometry()
{
  G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  G4RunManager::GetRunManager()->DefineWorldVolume(ConstructVolumes());
}
