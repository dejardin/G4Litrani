//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file electromagnetic/TestEm9/src/SteppingAction.cc
/// \brief Implementation of the SteppingAction class
//
// $Id: SteppingAction.cc 67268 2013-02-13 11:38:40Z ihrivnac $
//
//
/////////////////////////////////////////////////////////////////////////
//
// TestEm9: Crystal calorimeter
//
// Created: 31.01.03 V.Ivanchenko
//
// Modified:
//
////////////////////////////////////////////////////////////////////////
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "SteppingAction.hh"
#include "G4SteppingManager.hh"
#include "G4VTouchable.hh"
#include "HistoManager.hh"
#include "G4RunManager.hh"
#include "G4OpticalPhoton.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction():
  G4UserSteppingAction(),fHisto(HistoManager::GetPointer())
{
  fScintillationCounter = 0;
  fCerenkovCounter      = 0;
  fEventNumber = -1; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* aStep)
{
  G4int eventNumber = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  if (eventNumber != fEventNumber)
  {
    fEventNumber = eventNumber;
    fScintillationCounter = 0;
    fCerenkovCounter = 0;
  }


  G4Track* track = aStep->GetTrack();

  G4double edep = aStep->GetTotalEnergyDeposit()*track->GetWeight();
  //G4cout << "Deposited energy : " << aStep->GetTotalEnergyDeposit() << " " << track->GetWeight() << " " << edep << G4endl;
  if(edep == 0.) { return; }

  const G4VPhysicalVolume* pv = aStep->GetPreStepPoint()->GetPhysicalVolume();
  const G4LogicalVolume* lv = pv->GetLogicalVolume();

  G4int volumeIndex = -1;

  G4int copyNo = pv->GetCopyNo();

  // comparison of strings is not effective
  // this method left only for simplicity of the code
  G4String name = lv->GetName();
  if(name == "Crystal") volumeIndex = 0;
  //G4cout << "   vIndx= " << volumeIndex << " copyNo= " << copyNo << " " << name << G4endl;
  if(volumeIndex==0)
  {
    fHisto->AddStep();
    fHisto->AddEnergy(edep, volumeIndex, copyNo, track);
  }


  G4String ParticleName = track->GetDynamicParticle()-> GetParticleDefinition()->GetParticleName();
  //G4cout << " Particle name : " << ParticleName << G4endl;
  if (ParticleName == "opticalphoton") return;

  const std::vector<const G4Track*>* secondaries = aStep->GetSecondaryInCurrentStep();

  if (secondaries->size()>0)
  {
    for(unsigned int i=0; i<secondaries->size(); i++)
    {
      if (secondaries->at(i)->GetParentID()>0)
      {
        //G4cout << "My secondaries : "<< i << " parent : " << secondaries->at(i)->GetParentID() << " definition : " << secondaries->at(i)->GetCreatorProcess()->GetProcessName() << G4endl; 
        if(secondaries->at(i)->GetDynamicParticle()->GetParticleDefinition() == G4OpticalPhoton::OpticalPhotonDefinition())
        {
          if (secondaries->at(i)->GetCreatorProcess()->GetProcessName() == "Scintillation")fScintillationCounter++;
          if (secondaries->at(i)->GetCreatorProcess()->GetProcessName() == "Cerenkov")fCerenkovCounter++;
        }
      }
    }
  }
  G4cout << " Cumulative Cerenkov Photons : " << fCerenkovCounter << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


